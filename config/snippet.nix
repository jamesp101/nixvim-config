{
  plugins.luasnip = {
    enable = true;
  };
  plugins.cmp_luasnip = {
    enable = true;
  };
}
