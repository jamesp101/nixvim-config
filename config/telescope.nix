{
  plugins.telescope = {
    enable = true;
    keymaps = {
      "<leader><leader>" = "find_files";
      "<leader>pf" = "live_grep";
      "<C-x>b" = "buffers";
    };

    extensions. file_browser = {
        enable = true;
    };
    extensions.frecency = {
      enable = true;
    };
  };
  plugins.rainbow-delimiters = {
    enable = true;
  };
}
