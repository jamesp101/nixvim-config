{
  maps = {
    
    normal."<leader>.h" = {
      silent = true;
      action = "<cmd>TSToggle refactor.highlight_current_scope<CR>";
    };
    normal."<leader>.c" = {
      silent = true;
      action = "<cmd>TSContextToggle<CR>";
    };
    

    # Windows
    normal."<M-h>" = {
      silent = true;
      action = "<C-w><C-h>";
    };
    normal."<M-l>" = {
      silent = true;
      action = "<C-w><C-l>";
    };
    normal."<M-j>" = {
      silent = true;
      action = "<C-w><C-j>";
    };
    normal."<M-k>" = {
      silent = true;
      action = "<C-w><C-k>";
    };

    # Move selected line in visual mode
    visual."K" = {
      silent = true;
      action = ":move '<-2<CR>gv-gv";
    };
    visual."J" = {
      silent = true;
      action = ":move '>+2<CR>gv-gv";
    };


    # Indent Stay in visual mode
    visual.">" = {
      silent = true;
      action = ">gv";
    };
    visual."<" = {
      silent = true;
      action = "<gv";
    };

    # Center search results
    normal."n" = {
      silent = true;
      action = "nzz";
    };
    normal."N" = {
      silent = true;
      action = "Nzz";
    };

    # Terminal Escape
    terminal."<esc>" = {
        silent = true;
        action = "<C-\\><C-n>";
    };

    # Escape of insert mode with jk
    insert."jk" = {
        silent = true;
        action = "<esc>";
    };

  };

    



}
