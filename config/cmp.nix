{
  plugins.nvim-cmp = {
    enable = true;
    sources = [
        { name = "nvim_lsp"; }
        { name = "buffer"; }
        { name = "nvim_lsp_document_symbol"; }
        { name = "nvim_lsp_signature_help"; }
    ];
    mapping = {
      "<C-p>" = "cmp.mapping.select_prev_item()";
      "<C-n>" = "cmp.mapping.select_next_item()";
      "<C-d>" = "cmp.mapping.scroll_docs(-3)";
      "<C-f>" = "cmp.mapping.scroll_docs(4)";
      "<CR>"  = ''cmp.mapping.confirm({
        behavior = cmp.ConfirmBehavior.Replace,
        select = false
      })'';
    };
  };
  plugins.cmp-nvim-lsp = {
    enable = true;
  };
  plugins.cmp-nvim-lsp-document-symbol = {
    enable = true;
  };
  plugins.cmp-nvim-lsp-signature-help = {
    enable = true;
  };
  plugins.cmp-nvim-lua = {
    enable = true;
  };


}
