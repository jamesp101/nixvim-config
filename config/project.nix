{
  plugins.project-nvim = {
    enable = true;
  };
  plugins.telescope.extensions.project-nvim.enable = true;
}
