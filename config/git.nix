{
  plugins.neogit = {
    enable = true;
    useMagitKeybindings = true;
  };

  plugins.gitsigns = {
    enable = true;
  };


  maps = {
    normal."<leader>gg" = {
      silent = true;
      action = "<cmd>Neogit<CR>";
    };
  };
}
