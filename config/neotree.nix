{
  plugins.neo-tree = {
    enable = true;
  };

  maps = {
    normal."<leader>t" = {
      silent = true;
      action = "<cmd>Neotree reveal right<CR>";
    };
  };
}

