{
  plugins.toggleterm = {
    enable = true;
  };

  maps = {

    normal."<leader>`" = {
      silent = true;
      action = "<cmd>ToggleTerm<CR>";
    };
  };
}
