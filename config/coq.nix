{
  plugins.coq-nvim = {
    enable = true;
    autoStart = true;
    installArtifacts = true;
  };
}
