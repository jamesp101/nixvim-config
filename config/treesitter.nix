{
  plugins.treesitter = {
    enable = true;
    ensureInstalled = "all";
    # folding = true;
    indent = true;
    nixvimInjections = true;
    incrementalSelection.enable = true;
  };

  plugins.treesitter-context = {
    enable = true;
    lineNumbers = true;
  };

  plugins.treesitter-refactor = {
    enable = true;
    # highlightCurrentScope.enable = true;
    highlightDefinitions.enable = true;
    navigation.enable = true;
    smartRename.enable = true;
  };

  plugins.indent-blankline = {
    enable = true;
    useTreesitter = true;
    useTreesitterScope = true;
  };
}
