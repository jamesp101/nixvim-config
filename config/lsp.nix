{
  plugins.lsp = {
    enable = true;
    keymaps.lspBuf = {
      "gD" = "references";
      "gd" = "definition";
      "gi" = "implementation";
      "gt" = "type_definition";
    };

    servers = {
      astro.enable = true;
      lua-ls.enable = true;
      rnix-lsp.enable = true;
      tsserver.enable = true;
      zls.enable = true;
      svelte.enable = true;
      pyright.enable = true;
      nil_ls.enable = true;
      html.enable = true;
      clangd.enable = true;
      cssls.enable = true;
      bashls.enable = true;
    };
  };

  plugins.lspkind = {
    enable = true;
    cmp = {
      enable = true;
    };
  };
  plugins.fidget = {
    enable = true;
  };

  plugins.lspsaga = {
    enable = true;
    beacon = {
      enable = true;
    };
    codeAction = {
     extendGitSigns = true;
    };
    lightbulb.enable = true;
  };

  maps = {
      normal."K" = {
          silent = true;
          action = "<cmd>Lspsaga hover_doc<CR>";
      };
      normal."<leader>m" = {
          silent = true;
          action = "<cmd>Lspsaga outline<CR>";
      };
      normal."ld" = {
          silent = true;
          action = "<cmd>Lspsaga peek_definition<CR>";
      };
      normal."<leader>r" = {
          silent = true;
          action = "<cmd>Lspsaga rename<CR>";
      };
      normal."<leader>e" = {
          silent = true;
          action = "<cmd>Lspsaga show_workspace_diagnostics<CR>";
      };
      normal."<leader>F" = {
          silent = true;
          action = "<cmd>Lspsaga finder<CR>";
      };
      normal."<leader>f" = {
          silent = true;
          action = "<cmd>lua vim.lsp.buf.format { async = true }<CR>";
      };
      normal."<C-.>" = {
          silent = true;
          action = "<cmd>Lspsaga code_action<CR>";
      };
  };
}
