{
  globals.mapleader = " ";
  extraConfigLua = "${builtins.readFile ../opts.lua}";
  # Import all your configuration modules here
  imports = [
    ./colors.nix
    ./treesitter.nix
    ./keymaps.nix
    ./telescope.nix
    ./lsp.nix
    ./surround.nix
    ./cmp.nix
    ./comment.nix
    ./git.nix
    ./term.nix
    ./line.nix
    ./dashboard.nix
    ./project.nix
    ./dap.nix
    ./autopairs.nix
    ./neotree.nix
    # ./coq.nix
  ];

}
